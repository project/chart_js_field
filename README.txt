Chart.js Field


CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers


INTRODUCTION
------------

The Chart.js Field module creates a field type which uses the 
Chart.js javascript library. This module is different than 
other chart modules becuase it is main purpose is creating a 
field type. The Chart.js javascript library is used to allow 
content creators the ability to add charts to content easily
without many complicated options. 

This module requires the Chart.js javascript library to be 
installed in libraries/ directory. This module creates an 
easy to use interface for the users in filling out the data
for charts.


REQUIREMENTS
------------

This module requires the following library:

  * Chart.js (https://www.chartjs.org/)


INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal 
 module. Visit:
 https://drupal.org/documentation/install/modules-themes/modules-7
 for further information.

* Install the Chart.js javascript library. Visit:
https://www.chartjs.org/docs/latest/getting-started/installation.html


CONFIGURATION
-------------
 
 * Currently there is no configuration.


TROUBLESHOOTING
---------------

 * If the field is not rendering, check that the Chart.js library
 is in your libraries/ diectory and that the minified file is in
 libraries/chart.js/dist/Chart.min.js.


MAINTAINERS
-----------

Current maintainers:
 * Bobby Saul - https://www.drupal.org/u/bobbysaul
